FROM lscr.io/linuxserver/code-server


# Install basics
RUN \
  apt-get update --fix-missing \
  && apt-get install -y --no-install-recommends \
     git \
     openssh-client \
     python3 \
     python3-pip \
     unzip \
     neovim \
     zip \
     curl \
     lsb-release \
     gnupg \
     software-properties-common \
     tmux \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*


# Install docker CLI
RUN \
  apt-get -y install --no-install-recommends \
    ca-certificates \
    curl \
    gnupg \
    lsb-release \
  && curl -fsSL https://download.docker.com/linux/ubuntu/gpg | \
    sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg \    
  && echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] \
    https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee \
    /etc/apt/sources.list.d/docker.list > /dev/null \
  && apt-get update --fix-missing \
  && apt-get -y install docker-ce-cli \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*

# Install Miniconda
RUN curl -fsSL https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -o /tmp/miniconda.sh \
  && /bin/bash /tmp/miniconda.sh -b -p /opt/miniconda \
  && rm /tmp/miniconda.sh

# Update PATH to include Conda
ENV PATH=/opt/miniconda/bin:$PATH

# Initialize Conda for non-interactive shells
RUN conda init bash \
  && echo ". /opt/conda/etc/profile.d/conda.sh" >> ~/.bashrc

# Configure Conda channels and priorities
RUN conda config --add channels conda-forge \
  && conda config --set channel_priority strict
